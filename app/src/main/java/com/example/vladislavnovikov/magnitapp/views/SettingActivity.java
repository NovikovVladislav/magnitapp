package com.example.vladislavnovikov.magnitapp.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vladislavnovikov.magnitapp.R;
import com.example.vladislavnovikov.magnitapp.adapters.AdapterHistoryRecyclerView;
import com.example.vladislavnovikov.magnitapp.entities.ButtonItem;
import com.example.vladislavnovikov.magnitapp.managers.DatabaseHistoryHandler;
import com.example.vladislavnovikov.magnitapp.managers.StoreManager;

import java.util.ArrayList;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etRow;
    private EditText etFillPercent;
    private DatabaseHistoryHandler historyHandler;
    private ArrayList<ButtonItem> history;
    private AdapterHistoryRecyclerView adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setting);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etRow = (EditText) findViewById(R.id.et_row);
        etFillPercent = (EditText) findViewById(R.id.et_percent);

        Button btnOk = (Button) findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(this);

        initRecyclerView();
    }

    private void initRecyclerView() {

        historyHandler = new DatabaseHistoryHandler(this);
        history = new ArrayList<>();
        history.addAll(historyHandler.getAllHistory());

        RecyclerView recyclerViewHistory = (RecyclerView) findViewById(R.id.recycler_view_history);
        recyclerViewHistory.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AdapterHistoryRecyclerView(history);
        recyclerViewHistory.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        StoreManager storeManager = new StoreManager(this);

        if ((etRow.getText().toString().isEmpty()) || (etFillPercent.getText().toString().isEmpty())) {
            return;
        }

        int id = Integer.parseInt(etRow.getText().toString());
        float fillPercent = Float.parseFloat(etFillPercent.getText().toString());

        if ((id < 100) && (id >= 0)) {
            storeManager.changeFillButton(id, fillPercent);

            ButtonItem buttonItem = new ButtonItem(id, fillPercent);
            historyHandler.addItemHistory(buttonItem);
            history.add(buttonItem);
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(this, "Вводимые данные некорректны", Toast.LENGTH_SHORT).show();
        }

        etRow.setText("");
        etFillPercent.setText("");

    }
}
