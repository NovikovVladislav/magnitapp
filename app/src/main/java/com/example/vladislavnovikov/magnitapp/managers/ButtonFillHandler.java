package com.example.vladislavnovikov.magnitapp.managers;

import android.graphics.drawable.ClipDrawable;
import android.widget.Button;

/**
 * Created by VladislavNovikov on 25.06.17.
 */

public class ButtonFillHandler {

    public static void changeBtn(Button btn, float fillPercent) {
        int lvl = (int) (fillPercent * 10000.0f);
        ClipDrawable drawable = (ClipDrawable) btn.getBackground();
        drawable.setLevel(lvl);
    }

}
