package com.example.vladislavnovikov.magnitapp.entities;

/**
 * Created by VladislavNovikov on 25.06.17.
 */

public class ButtonItem {

    private int id;
    private float percentFill;

    public ButtonItem(int id, float percentFill) {
        this.id = id;
        this.percentFill = percentFill;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPercentFill() {
        return percentFill;
    }

    public void setPercentFill(float percentFill) {
        this.percentFill = percentFill;
    }
}
