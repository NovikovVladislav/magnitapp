package com.example.vladislavnovikov.magnitapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.vladislavnovikov.magnitapp.managers.ButtonFillHandler;
import com.example.vladislavnovikov.magnitapp.R;
import com.example.vladislavnovikov.magnitapp.entities.ButtonItem;
import com.example.vladislavnovikov.magnitapp.entities.SingleItemBtn;
import com.example.vladislavnovikov.magnitapp.views.DescriptionBtnActivity;

import java.util.ArrayList;

/**
 * Created by VladislavNovikov on 24.06.17.
 */

public class AdapterMainRecyclerView extends RecyclerView.Adapter<AdapterMainRecyclerView.ViewHolder> {

    private ArrayList<ButtonItem> items;

    public AdapterMainRecyclerView() {
    }

    public void setItems(ArrayList<ButtonItem> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rel_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String idBtn = String.valueOf(items.get(position).getId());

        ButtonFillHandler.changeBtn(holder.btn, items.get(position).getPercentFill());
        holder.idBtn.setText(idBtn);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView idBtn;
        private Button btn;
        private RelativeLayout containerItem;

        public ViewHolder(View itemView) {
            super(itemView);

            idBtn = (TextView) itemView.findViewById(R.id.id_btn);
            btn = (Button) itemView.findViewById(R.id.btn);

            containerItem = (RelativeLayout) itemView.findViewById(R.id.container_item);
            containerItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            SingleItemBtn.getInstance().setIdBtn(Integer.parseInt(idBtn.getText().toString()));
            context.startActivity(new Intent(context, DescriptionBtnActivity.class));
        }
    }
}
