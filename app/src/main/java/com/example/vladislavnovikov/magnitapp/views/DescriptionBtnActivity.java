package com.example.vladislavnovikov.magnitapp.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.example.vladislavnovikov.magnitapp.R;
import com.example.vladislavnovikov.magnitapp.entities.SingleItemBtn;
import com.example.vladislavnovikov.magnitapp.managers.ButtonFillHandler;
import com.example.vladislavnovikov.magnitapp.managers.StoreManager;

public class DescriptionBtnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_btn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int id = SingleItemBtn.getInstance().getIdBtn();

        Button btn = (Button) findViewById(R.id.btn_description);
        ButtonFillHandler.changeBtn(btn, new StoreManager(this).getFillPercentById(id));

        TextView idBtn = (TextView) findViewById(R.id.id_btn_description);
        idBtn.setText(String.valueOf(id));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
