package com.example.vladislavnovikov.magnitapp.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.vladislavnovikov.magnitapp.entities.ButtonItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VladislavNovikov on 25.06.17.
 */

public class StoreManager {

    private static final String PREFERENCES = "setting";
    private static final String STATE_BUTTON = "buttons";

    private SharedPreferences profile;
    private SharedPreferences.Editor editor;

    public StoreManager(Context context) {
        profile = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        editor = profile.edit();
    }

    public ArrayList<ButtonItem> getBtnArray() {
        String jsonArray = profile.getString(STATE_BUTTON, "");

        if (jsonArray == "") {
            return getClearArray();
        }

        return new Gson().fromJson(jsonArray,
                new TypeToken<List<ButtonItem>>() {
                }.getType());
    }


    public void changeFillButton(int id, float fillPercent) {
        ArrayList<ButtonItem> items = getBtnArray();
        items.get(id).setPercentFill(fillPercent);
        editor.putString(STATE_BUTTON, new Gson().toJson(items));
        editor.apply();
    }

    public float getFillPercentById(int id) {
        return getBtnArray().get(id).getPercentFill();
    }

    private ArrayList<ButtonItem> getClearArray() {

        ArrayList<ButtonItem> items = new ArrayList<>(100);
        for (int i = 0; i < 100; i++) {
            items.add(new ButtonItem(i, 0f));
        }

        editor.putString(STATE_BUTTON, new Gson().toJson(items));
        editor.apply();
        return items;
    }
}
