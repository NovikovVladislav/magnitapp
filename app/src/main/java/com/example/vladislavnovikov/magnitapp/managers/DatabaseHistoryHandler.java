package com.example.vladislavnovikov.magnitapp.managers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vladislavnovikov.magnitapp.entities.ButtonItem;

import java.util.ArrayList;

/**
 * Created by VladislavNovikov on 25.06.17.
 */

public class DatabaseHistoryHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "magnitApp_deb0";
    private static final String TABLE_HISTORY = "history";

    private static final String KEY_ID = "id";
    private static final String KEY_ID_BTN = "id_btn";
    private static final String KEY_PERCENT = "percent";

    public DatabaseHistoryHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_HISTORY + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_ID_BTN + " INTEGER, "
                + KEY_PERCENT + " REAL" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        onCreate(db);
    }

    public void addItemHistory(ButtonItem buttonItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_BTN, buttonItem.getId());
        values.put(KEY_PERCENT, buttonItem.getPercentFill());

        db.insert(TABLE_HISTORY, null, values);
        db.close();
    }

    public ArrayList<ButtonItem> getAllHistory() {
        ArrayList<ButtonItem> buttonItems = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_HISTORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                int id = Integer.parseInt(cursor.getString(1));
                float percent = Float.parseFloat(cursor.getString(2));

                buttonItems.add(new ButtonItem(id, percent));
            } while (cursor.moveToNext());
        }

        return buttonItems;
    }

    public int getItemHistoryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_HISTORY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }
}
