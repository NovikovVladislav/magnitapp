package com.example.vladislavnovikov.magnitapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.vladislavnovikov.magnitapp.R;
import com.example.vladislavnovikov.magnitapp.entities.ButtonItem;
import com.example.vladislavnovikov.magnitapp.managers.ButtonFillHandler;

import java.util.ArrayList;

/**
 * Created by VladislavNovikov on 24.06.17.
 */

public class AdapterHistoryRecyclerView extends RecyclerView.Adapter<AdapterHistoryRecyclerView.ViewHolder> {

    private ArrayList<ButtonItem> items;

    public AdapterHistoryRecyclerView(ArrayList<ButtonItem> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rel_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String idBtn = String.valueOf(items.get(position).getId());

        ButtonFillHandler.changeBtn(holder.btn, items.get(position).getPercentFill());
        holder.idBtn.setText(idBtn);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView idBtn;
        private Button btn;

        public ViewHolder(View itemView) {
            super(itemView);

            idBtn = (TextView) itemView.findViewById(R.id.id_btn);
            btn = (Button) itemView.findViewById(R.id.btn);
        }
    }
}
