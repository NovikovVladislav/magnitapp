package com.example.vladislavnovikov.magnitapp.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.vladislavnovikov.magnitapp.adapters.AdapterMainRecyclerView;
import com.example.vladislavnovikov.magnitapp.R;
import com.example.vladislavnovikov.magnitapp.managers.StoreManager;

public class MainActivity extends AppCompatActivity {

    private AdapterMainRecyclerView adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
    }

    private void initRecyclerView() {
        RecyclerView recyclerViewBtn = (RecyclerView) findViewById(R.id.recycler_view_btn);
        recyclerViewBtn.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AdapterMainRecyclerView();
        recyclerViewBtn.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        StoreManager storeManager = new StoreManager(this);
        adapter.setItems(storeManager.getBtnArray());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setting:
                startActivity(new Intent(this, SettingActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
