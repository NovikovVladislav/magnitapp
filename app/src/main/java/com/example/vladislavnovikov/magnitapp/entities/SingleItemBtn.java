package com.example.vladislavnovikov.magnitapp.entities;

/**
 * Created by VladislavNovikov on 24.06.17.
 */

public class SingleItemBtn {

    private static SingleItemBtn ourInstance = new SingleItemBtn();
    private Integer idBtn;

    public static SingleItemBtn getInstance() {
        return ourInstance;
    }

    public void setIdBtn(int idBtn) {
        ourInstance.idBtn = idBtn;
    }

    public Integer getIdBtn() {
        return ourInstance.idBtn;
    }

    private SingleItemBtn() {
    }


}
